CARGO = cargo
ifndef PREFIX
	PREFIX = /usr/local
endif

all: target/release/golterm

target/release/golterm:
	PREFIX=$(PREFIX) $(CARGO) build --release

clean:
	cargo clean

install: all
	mkdir -p $(PREFIX)/bin
	mkdir -p $(PREFIX)/share/golterm
	cp -r share/golterm/* $(PREFIX)/share/golterm
	install -s target/release/golterm $(PREFIX)/bin/golterm

uninstall:
	rm -r $(PREFIX)/share/golterm
	rm $(PREFIX)/bin/golterm
