use crate::error;
use std::path::Path;
use std::fs::{read_to_string, read_dir};
use serde::Deserialize;

const PREFIX: Option<&str> = option_env!("PREFIX");

#[derive(Deserialize)]
pub struct YamlFile {
    pub name: String,
    pub description: String,
    pub data: String,
}

impl YamlFile {
    pub fn from_file(path: &Path) -> error::Result<YamlFile> {
        let file = read_to_string(path)?;
        Ok(serde_yaml::from_str::<YamlFile>(file.as_str())?)
    }
}

pub fn get_patterns() -> error::Result<Vec<YamlFile>> {
    let prefix = match PREFIX { Some(prefix) => prefix, None => "." };
    let shared = Path::new(prefix).join("share/golterm");

    read_dir(shared)?.collect::<Result<Vec<_>, _>>()?
        .iter().map(|file| { YamlFile::from_file(&file.path()) }).collect::<Result<Vec<_>, _>>()
}

pub fn list_patterns() -> error::Result<()> {
    let yaml_files = get_patterns()?;
    let max = yaml_files.iter().map(|yaml| { yaml.name.len() }).max().unwrap_or(0);

    println!("Available patterns:");
    for file in yaml_files {
        println!("    {:width$}   {}", file.name, file.description, width = max);
    }

    Ok(())
}

