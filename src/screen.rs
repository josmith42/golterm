mod status_bar;
mod colony_view;

use std::io::{Write, stdout};
use std::error::Error;
use crossterm::{execute, Result, terminal::{EnterAlternateScreen, LeaveAlternateScreen}};
use crossterm::cursor;
use crossterm::queue;
use crossterm::style::ResetColor;
use crossterm::terminal::{Clear, ClearType, enable_raw_mode, disable_raw_mode, size};
use crossterm::event::{self, Event, KeyCode};
use std::time::Duration;
use crate::colony::Colony;
use crate::screen::status_bar::StatusBar;
use crate::screen::colony_view::{ScreenCell, ColonyView};

#[derive(PartialEq)]
enum Mode {
    WaitForEvent,
    Animating,
    Quit,
}

pub fn field_size((width, height): ScreenCell) -> ScreenCell {
    (width, height - 1)
}

pub struct Screen {
    colony: Colony,
    mode: Mode,

    colony_view: ColonyView,
    status_bar: StatusBar,
}

impl Drop for Screen {
    fn drop(&mut self) {
        execute!(stdout(), cursor::Show, LeaveAlternateScreen).unwrap();
        disable_raw_mode().unwrap();
    }
}

impl Screen {
    pub fn new(colony: Colony) -> Result<Self> {
        execute!(stdout(), EnterAlternateScreen, cursor::Hide)?;
        enable_raw_mode()?;
        let screen_size = size()?;
        let (width, height) = screen_size;

        Ok(Self {
            colony,
            mode: Mode::WaitForEvent,
            colony_view: ColonyView::new(field_size(screen_size)),
            status_bar: StatusBar::new(width, height - 1),
        })
    }

    fn draw(&mut self, erase: bool) -> core::result::Result<(), Box<dyn Error>> {
        queue!(stdout(), ResetColor)?;
        if erase {
            queue!(stdout(), Clear(ClearType::All))?;
        }
        self.colony_view.draw(&self.colony, erase)?;
        self.status_bar.draw()?;
        stdout().flush()?;
        Ok(())
    }

    fn on_resize(&mut self, width: u16, height: u16) -> core::result::Result<(), Box<dyn Error>> {
        self.colony_view.set_size(field_size((width, height)));
        self.status_bar.set_position(height - 1, width);
        self.draw(true)?;
        Ok(())
    }

    fn handle_event(&mut self) -> core::result::Result<(), Box<dyn Error>> {
        match event::read()? {
            Event::Key(evt) => match evt.code {
                KeyCode::Char('n') => self.next_generation()?,
                KeyCode::Char(' ') => self.mode = if self.mode == Mode::Animating { Mode::WaitForEvent } else { Mode::Animating },
                KeyCode::Char('q') => self.mode = Mode::Quit,
                KeyCode::Char('r') => self.reset()?,
                _ => {}
            }
            Event::Resize(x, y) => self.on_resize(x, y)?,
            _ => {}
        }
        Ok(())
    }

    fn next_generation(&mut self) -> core::result::Result<(), Box<dyn Error>> {
        self.colony.generation();
        self.status_bar.set_generation(self.colony.num_generations());
        self.draw(false)?;
        Ok(())
    }

    fn reset(&mut self) -> core::result::Result<(), Box<dyn Error>> {
        self.colony.reset();
        self.status_bar.set_generation(self.colony.num_generations());
        self.draw(false)?;
        Ok(())
    }

    fn animate(&mut self) -> core::result::Result<(), Box<dyn Error>> {
        if event::poll(Duration::from_millis(100))? {
            self.handle_event()?;
        } else {
            self.next_generation()?;
        }
        Ok(())
    }

    pub fn run_loop(&mut self) -> core::result::Result<(), Box<dyn Error>> {
        self.draw(true)?;
        loop {
            match self.mode {
                Mode::WaitForEvent => self.handle_event()?,
                Mode::Animating => self.animate()?,
                Mode::Quit => break
            }
        }
        Ok(())
    }
}
