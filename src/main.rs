mod file;
mod colony;
mod screen;
mod command;
mod error;

use crate::screen::Screen;
use crate::command::Command;
use crate::colony::Colony;
use crate::file::list_patterns;

use crossterm::terminal;
use crossterm::style::Color;
use crossterm::style::SetForegroundColor;

use std::error::Error;

fn run() -> Result<(), Box<dyn Error>> {
    let (max_x, max_y) = screen::field_size(terminal::size()?);

    match Command::from_command_line()? {
        Command::List => list_patterns()?,
        Command::Colony(colony_command) => {
            let colony = Colony::from(colony_command, (max_x as i32, max_y as i32))?;
            let mut screen = Screen::new(colony)?;
            screen.run_loop()?;
        }
    }
    Ok(())
}

fn main() {
    match run() {
        Ok(()) => {}
        Err(e) => eprintln!("{}error:{} {}", SetForegroundColor(Color::Red), SetForegroundColor(Color::Reset), e)
    }
}
