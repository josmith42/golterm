use std::collections::{HashSet, HashMap};
use crate::command::{ColonyCommand, FillCommand};
use crate::error::{Result, GoltError};
use crate::file::{self, YamlFile};
use rand::Rng;
use std::path::Path;

pub type Cell = (i32, i32);
type Collection = HashSet<Cell>;

pub struct Colony {
    orig_collection: Collection,
    collection: Collection,
    num_generations: u32
}

fn neighbours(&(x, y): &Cell) -> Vec<Cell> {
    vec![
        (x - 1, y - 1), (x, y - 1), (x + 1, y - 1),
        (x - 1, y), (x + 1, y),
        (x - 1, y + 1), (x, y + 1), (x + 1, y + 1),
    ]
}

impl Colony {
    pub fn from(command: ColonyCommand, max_xy: Cell) -> Result<Self> {
        match command {
            ColonyCommand::Fill(command) => Ok(Self::from_fill(command, max_xy)),
            ColonyCommand::File(filename) => Self::from_file(Path::new(&filename)),
            ColonyCommand::Pattern(pattern_name) => Self::from_pattern(&pattern_name),
        }
    }

    pub fn collection(&self) -> &Collection {
        &self.collection
    }

    pub fn num_generations(&self) -> u32 {
        self.num_generations
    }

    fn from_yaml(yaml_file: &YamlFile) -> Self {
        let mut collection = Collection::new();

        let mut x = 0;
        let mut y = 0;
        for c in yaml_file.data.chars() {
            match c {
                'x' => {
                    collection.insert((x, y));
                    x += 1;
                }
                '\n' => {
                    x = 0;
                    y += 1;
                }
                _ => x += 1
            }
        }
        Self::new(collection)
    }

    fn from_file(filename: &Path) -> Result<Self> {
        let yaml_file = YamlFile::from_file(filename)?;
        Ok(Self::from_yaml(&yaml_file))
    }

    fn from_pattern(pattern_name: &String) -> Result<Self> {
        let pattern_list = file::get_patterns()?;
        match pattern_list.iter().find(|p| { p.name == *pattern_name}) {
            None => GoltError::new(&format!("'{}' is not a valid pattern. (Run with -l to list patterns.)", pattern_name)),
            Some(file) => Ok(Self::from_yaml(file))
        }
    }

    fn new(collection: Collection) -> Self {
        Self {
            orig_collection: collection.clone(),
            collection,
            num_generations: 0
        }
    }

    fn from_fill(command: FillCommand, max_xy: Cell) -> Self {
        let mut collection = Collection::new();

        let mut rng = rand::thread_rng();
        let (max_x, max_y) = max_xy;
        let always_fill = command.percentage == 100;
        let percentage = command.percentage as f64 / 100.0;

        for y in 0..max_y {
            for x in 0..max_x {
                if always_fill || rng.gen_bool(percentage) {
                    collection.insert((x, y));
                }
            }
        }
        Self::new(collection)
    }

    fn neighbour_counts(&self) -> HashMap<Cell, i32> {
        let mut ncnts = HashMap::new();
        for cell in self.collection.iter().flat_map(neighbours) {
            *ncnts.entry(cell).or_insert(0) += 1;
        }
        ncnts
    }

    pub fn generation(&mut self) {
        self.collection = self.neighbour_counts()
            .into_iter()
            .filter_map(|(cell, cnt)|
                match (cnt, self.collection.contains(&cell)) {
                    (2, true) | (3, ..) => Some(cell),
                    _ => None
                })
            .collect();
        self.num_generations += 1;
    }

    pub fn reset(&mut self) {
        self.collection = self.orig_collection.clone();
        self.num_generations = 0;
    }
}

