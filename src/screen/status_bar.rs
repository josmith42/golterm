use crossterm::cursor::{MoveTo, MoveToColumn};
use crossterm::queue;
use crossterm::style::{Attribute, Color, Print, SetAttribute, SetForegroundColor, SetBackgroundColor};
use std::io::{Write, stdout};
use crate::error::Result;
use crossterm::terminal::{Clear, ClearType};

pub struct StatusBar {
    generation_count: u32,
    y: u16,
    width: u16,
}

impl StatusBar {
    pub fn new(width: u16, y: u16) -> Self {
        Self { generation_count: 0, y, width }
    }

    pub fn set_position(&mut self, y: u16, width: u16) {
        self.y = y;
        self.width = width
    }

    pub fn set_generation(&mut self, generation: u32) {
        self.generation_count = generation;
    }

    pub fn draw(&mut self) -> Result<()> {
        queue!(stdout(),
            MoveTo(0, self.y),
            SetBackgroundColor(Color::DarkBlue),
            Clear(ClearType::UntilNewLine)
        )?;
        let mut descriptions_width = 0;
        for (key, description) in vec![
            ("[q]", "Quit "),
            ("[SPC]", "Animate "),
            ("[n]", "Next "),
            ("[r]", "Reset")
        ] {
            let description_width = key.len() + description.len();
            if descriptions_width + description_width > self.width.into() {
                break;
            }
            queue!(
                stdout(),
                SetAttribute(Attribute::Bold), SetForegroundColor(Color::White), Print(key),
                SetAttribute(Attribute::NormalIntensity), SetForegroundColor(Color::Grey), Print(description),
            )?;
            descriptions_width += description_width;
        }

        let gen_str = format!("Generation: {} ", self.generation_count);
        if gen_str.len() >= self.width.into() {
            return Ok(());
        }
        if descriptions_width + gen_str.len() >= self.width.into() {
            queue!(
                stdout(),
                MoveToColumn(self.width - gen_str.len() as u16),
                Print(">")
            )?;
        }

        queue!(
            stdout(),
            MoveToColumn(self.width + 1 - gen_str.len() as u16),
            Print(gen_str))?;

        Ok(())
    }
}