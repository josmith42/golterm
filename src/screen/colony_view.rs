use crate::colony::{Colony, Cell};
use crate::error::Result;

use crossterm::queue;
use crossterm::cursor::{MoveTo, MoveToColumn};
use crossterm::style::{Print, SetForegroundColor, Color};
use std::collections::{HashMap, HashSet};
use std::io::{Write, stdout};

pub type ScreenCell = (u16, u16);

#[derive(Copy, Clone)]
enum Action {
    TurnOn,
    TurnOff,
}

struct CellViewModel {
    cell: ScreenCell,
    action: Action,
}

struct RowItem {
    pos: u16,
    action: Action
}

pub struct ColonyView {
    size: ScreenCell,
    cached_colony: HashSet<ScreenCell>,
}

impl ColonyView {
    pub fn new(size: ScreenCell) -> Self {
        Self { size, cached_colony: HashSet::new() }
    }

    pub fn set_size(&mut self, size: ScreenCell) {
        self.size = size;
    }

    fn is_on_screen(&self, (x, y): Cell) -> bool {
        if x < 0 || y < 0 {
            return false;
        }

        let (width, height) = self.size;
        x < width as i32 && y < height as i32
    }

    pub fn draw(&mut self, colony: &Colony, redraw: bool) -> Result<()> {
        queue!(stdout(), SetForegroundColor(Color::Green))?;
        if redraw {
            self.cached_colony.clear();
        }
        let screen_colony: HashSet<(u16, u16)> = colony.collection().iter()
            .filter(|cell| { self.is_on_screen(**cell) })
            .map(|(x, y)| { (*x as u16, *y as u16) })
            .collect();
        let turn_off = self.cached_colony.difference(&screen_colony)
            .map(|cell| { CellViewModel { cell: *cell, action: Action::TurnOff } });
        let turn_on = screen_colony.difference(&self.cached_colony)
            .map(|cell| { CellViewModel { cell: *cell, action: Action::TurnOn } });
        let cell_view_models: Vec<CellViewModel> = turn_off.chain(turn_on).collect();

        let mut row_map: HashMap<u16, Vec<RowItem>> = HashMap::new();
        for cell_view_model in cell_view_models.iter() {
            let (x, y) = cell_view_model.cell;
            let row = row_map.entry(y).or_insert(Vec::new());
            row.push(RowItem { pos: x, action: cell_view_model.action});
        }

        for (y, row) in row_map.iter() {
            queue!(stdout(), MoveTo(0, *y))?;
            let mut row: Vec<&RowItem> = row.iter().collect();
            row.sort_unstable_by(|row_item, other| { row_item.pos.cmp(&other.pos) });
            let groups = consecutive_slices(&row)
                .map(|row_items| {
                    (row_items[0].pos, row_items.iter().map(|item| {item.action}))
                });
            for (pos, actions) in groups {
                queue!(stdout(), MoveToColumn(pos + 1))?;
                for action in actions {
                    match action {
                        Action::TurnOn => queue!(stdout(), Print("█"))?,
                        Action::TurnOff => queue!(stdout(), Print(" "))?,
                    };
                }
            }
        }
        self.cached_colony = screen_colony;
        Ok(())
    }
}

fn consecutive_slices<'a, 'b>(data: &'a [&'b RowItem]) -> impl Iterator<Item=&'a [&'b RowItem]> {
    let mut slice_start = 0;
    (1..data.len() + 1).flat_map(move |i| {
        if i == data.len() || data[i - 1].pos + 1 != data[i].pos {
            let begin = slice_start;
            slice_start = i;
            Some(&data[begin..i])
        } else {
            None
        }
    })
}
