use clap::{App, crate_version, crate_authors, crate_name, Arg, ArgMatches, value_t_or_exit};
use crate::error::{Result, GoltError};
use std::process::exit;
use std::io::{stderr, Write};

const ARG_PATTERN: &str = "PATTERN";
const ARG_PATTERN_LONG: &str = "pattern";
const ARG_LIST: &str = "list";
const ARG_RANDOM: &str = "RANDOM";
const ARG_RANDOM_LONG: &str = "random";
const ARG_SATURATE: &str = "saturate";
const ARG_FILE: &str = "FILE";
const ARG_FILE_LONG: &str = "file";

fn parse_command_line<'a, 'b>() -> App<'a, 'b> {
    App::new("Game Of Life for Terminal")
        .about("Conway's Game of Life for terminal")
        .version(crate_version!())
        .author(crate_authors!())
        .name(crate_name!())

        .arg(Arg::with_name(ARG_PATTERN)
            .long(ARG_PATTERN_LONG)
            .short("p")
            .takes_value(true)
            .help("Specifies starting pattern")
        )

        .arg(Arg::with_name(ARG_LIST)
            .long(ARG_LIST)
            .short("l")
            .help("List patterns")
        )

        .arg(Arg::with_name(ARG_RANDOM)
            .long(ARG_RANDOM_LONG)
            .short("r")
            .takes_value(true)
            .default_value("50")
            .help("Generates a random starting generation")
        )

        .arg(Arg::with_name(ARG_SATURATE)
            .long(ARG_SATURATE)
            .short("s")
            .help("Fills (saturates) the screen with live cells")
        )

        .arg(Arg::with_name(ARG_FILE)
            .long(ARG_FILE_LONG)
            .short("f")
            .takes_value(true)
            .help("Read starting generation from a file")
        )
}

struct Usage {
    message: Vec<u8>
}

impl Usage {
    fn from(app: &mut App) -> Result<Self> {
        let mut message = Vec::new();
        app.write_long_help(&mut message)?;
        Ok(Self { message })
    }

    fn print_and_exit(&mut self) -> ! {
        stderr().write(&mut self.message).unwrap_or_else(|_| exit(1));
        exit(1);
    }
}

fn check_occurrence<'a>(args: &ArgMatches, options: &mut Vec<&'a str>, option: &'a str, name: Option<&'static str>) {
    if args.occurrences_of(option) >= 1 {
        options.push(match name { Some(name) => name, None => option });
    }
}

fn check_for_conflicts(args: &ArgMatches) -> Result<()> {
    let mut options = Vec::new();
    check_occurrence(args, &mut options, ARG_PATTERN, Some(ARG_PATTERN_LONG));
    check_occurrence(args, &mut options, ARG_LIST, None);
    check_occurrence(args, &mut options, ARG_FILE, Some(ARG_FILE_LONG));
    check_occurrence(args, &mut options, ARG_RANDOM, Some(ARG_RANDOM_LONG));
    check_occurrence(args, &mut options, ARG_SATURATE, None);

    if options.len() > 1 {
        GoltError::new(&format!("Options --{} and --{} cannot be used together", options[0], options[1]))
    } else {
        Ok(())
    }
}

pub struct FillCommand {
    pub percentage: u32,
}

pub enum ColonyCommand {
    Fill(FillCommand),
    File(String),
    Pattern(String)
}

pub enum Command {
    List,
    Colony(ColonyCommand)
}

impl Command {
    pub fn from_command_line() -> Result<Command> {
        let mut app = parse_command_line();
        // Have to get the usage string from App before calling get_matches() because
        // get_matches() takes ownership of App.
        let mut usage = Usage::from(&mut app)?;

        let args = app.get_matches();

        check_for_conflicts(&args)?;

        if args.is_present(ARG_PATTERN) {
            Self::from_pattern(&args)
        } else if args.is_present(ARG_LIST) {
            Ok(Self::List)
        } else if args.is_present(ARG_FILE) {
            Self::from_file(&args)
        } else if args.occurrences_of(ARG_RANDOM) == 1 {
            Self::from_random(&args)
        } else if args.is_present(ARG_SATURATE) {
            Self::from_saturate()
        } else {
            usage.print_and_exit();
        }
    }

    fn from_pattern(args: &ArgMatches) -> Result<Command> {
        let pattern_name = args.value_of(ARG_PATTERN).expect("pattern argument not found");
        Ok(Command::Colony(ColonyCommand::Pattern(String::from(pattern_name))))
    }

    fn from_file(args: &ArgMatches) -> Result<Command> {
        let filename = args.value_of(ARG_FILE).expect("filename argument not found");
        Ok(Command::Colony(ColonyCommand::File(String::from(filename))))
    }

    fn from_random(args: &ArgMatches) -> Result<Command> {
        let percentage = value_t_or_exit!(args.value_of(ARG_RANDOM), u32);
        if percentage > 100 {
            return GoltError::new(&format!("Percentage value for percentage out of range: {}", percentage));
        }
        Ok(Command::Colony(ColonyCommand::Fill(FillCommand { percentage })))
    }

    fn from_saturate() -> Result<Command> {
        Ok(Command::Colony(ColonyCommand::Fill(FillCommand { percentage: 100 })))
    }
}

