# Golterm - Game of Life for Terminal

`golterm` is a console based Game-of-Life. Give it a starting configuration, and it will compute each successive generation and display it.

## Installation

### From Source
Rust is required to build from source. Get the latest rust build environment with `rustup` at https://rustup.rs. Then run the following commands:
```
git clone https://gitlab.com/josmith42/golt
cd golt
make
sudo make install # sudo not required on MacOS
```
This will install `golterm` to `/usr/local/bin` with a handful of shared files to `/usr/local/bin/golterm`.

To uninstall:
```
sudo make uninstall
```

If you would like to install to a different path, e.g., `/usr`, issue the following commands instead:
```
make PREFIX=/usr
sudo make install PREFIX=/usr
```
